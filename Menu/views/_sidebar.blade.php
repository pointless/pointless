@foreach($items as $item)
    @if($item->isHeader())
        @if(! str_replace(['-', '_'], '', $item->title))
            <li class="list-divider"></li>
        @else 
            <li class="list-header">{!! $item->title !!}</li>
        @endif
    @else
        <li 
            @if($item->isActive)
                {!! htmlattributes(['class' => $item->withItems() ? 'active-sub active' : 'active-link']) !!}
            @endif
        >
            <a href="{{ $item->url ?? '#' }}" {!! htmlattributes($item->attributes) !!} >
                @if($item->icon)
                    <i class="{{ $item->icon }}"></i>
                @endif
                <span class="menu-title">
                    <strong>{!! $item->title !!}</strong>
                </span>
                @if($item->withItems())
                    <i class="arrow"></i>
                @endif
            </a>
            @if($item->withItems())
                <ul class="collapse @if($item->isActive) in @endif ">
                    @include('menu::_sidebar', ['items' => $item->items])
                </ul>
            @endif
        </li>
    @endif
    
@endforeach