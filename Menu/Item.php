<?php

namespace Pointless\Menu;

use Illuminate\Support\Facades\Route;

class Item
{
    public $menu;

    public $key;
    public $title;
    public $options;
    public $icon;
    public $url;
    public $attributes;
    public $permission;
    public $items;
    public $isActive = false;

    protected static $rootUrl;

    public static function populateItems(Menu $menu, array $items)
    {
        return collect($items)->map(function ($value, $key) use ($menu) {
            $title = is_int($key) && is_string($value) ? $value : $key;
            $options = is_string($key) ? (is_array($value) ? $value : ['url' => $value]) : [];
            return new static($menu, kebab_case($key), $options['title'] ?? $title, $options);
        })->filter->isAuthorized()
            ->reject->withoutItems()
            ->map->setActiveStatus()
            ->values();
    }

    public function __construct(Menu $menu, $key, $title, $options = [])
    {
        $this->key = $key;
        $this->menu = $menu;
        $this->title = $title;
        $this->setOptions($options);
    }

    public function isHeader()
    {
        return is_null($this->url) && is_null($this->items);
    }

    public function setActiveStatus()
    {
        $this->isActive = $this->checkIsActive();
        return $this;
    }

    public function relativeUrl($url)
    {
        if (is_null(static::$rootUrl)) {
            static::$rootUrl = url('/');
        }
        $url = head(explode('?', $url));
        return starts_with($url, static::$rootUrl) ? str_start(substr($url, strlen(static::$rootUrl)), '/') : $url;
    }

    protected function matchUrl($pattern, $url = null)
    {
        $url = $url ?? $this->menu->request()->fullUrl();
        return str_is($this->relativeUrl($pattern), $this->relativeUrl($url));
    }

    protected function checkActiveByFullUrl()
    {
        return $this->matchUrl($this->url);
    }

    protected function checkActiveByUrl(...$acceptUrls)
    {
        foreach ($acceptUrls as $url) {
            if ($this->matchUrl($url)) {
                return true;
            }
        }
        return false;
    }

    protected function checkActiveByRoute(...$acceptRouteNames)
    {
        $currentRouteName = Route::currentRouteName();
        return !!collect($acceptRouteNames)->first(function ($routeName) use ($currentRouteName) {
            return str_is($routeName, $currentRouteName);
        });
    }

    protected function checkIsActive()
    {
        $acceptUrls = array_wrap($this->options['activeUrl'] ?? []);
        $acceptRoutes = array_wrap($this->options['activeRoute'] ?? []);
        $active = $this->checkActiveByFullUrl() ||
            $this->checkActiveByUrl(...$acceptUrls) ||
            $this->checkActiveByRoute(...$acceptRoutes);

        if (!$active && $this->withItems() && $this->items->filter->isActive->isNotEmpty()) {
            $active = true;
        }

        return $active;
    }

    public function setOptions(array $options)
    {
        $this->options = $options;
        $this->attributes = $options['attributes'] ?? [];
        $this->icon = $options['icon'] ?? null;
        $this->permission = $options['permission'] ?? null;
        if (isset($options['route'])) {
            $this->options['url'] = $this->getUrlFromRoute($options['route']);
        }
        $this->url = isset($this->options['url']) ? asset(...array_wrap($this->options['url'])) : null;
        $this->items = isset($options['items']) ? static::populateItems($this->menu, $options['items']) : null;
        return $this;
    }

    protected function getUrlFromRoute($route)
    {
        $route = array_wrap($route);
        $routeName = array_shift($route);
        $routeParams = array_shift($route);

        return route($routeName, $routeParams ?? [], false);
    }

    protected function isAuthorizedByPermission()
    {
        return $this->permission ? auth()->user()->hasEitherPermission(...array_wrap($this->permission)) : true;
    }

    public function isAuthorized()
    {
        $authorized = $this->isAuthorizedByPermission();
        $authorize = $this->options['authorize'] ?? null;
        if (isset($authorize) && is_callable($authorize)) {
            $authorized = call_user_func($authorize, $authorized, $this);
        }
        return $authorized;
    }

    public function withoutItems()
    {
        return !$this->url && $this->items && $this->items->isEmpty();
    }

    public function withItems()
    {
        return $this->items && $this->items->isNotEmpty();
    }
}
