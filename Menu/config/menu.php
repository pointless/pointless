<?php

return [
    'layout' => [
        'layouts.app' => ['sidebar'],
    ],

    'menus' => [
        'sidebar' => [
            'Navigation', // header
            'Home' => [
                'icon' => 'nifty-pli-home',
                'url' => 'home',
            ],
        /*
            '---', // divider
            'Performance' => [
                'icon'  => 'nifty-psi-bar-chart',
                'items' => [
                    'Balance Scorecard' => [
                        'permission' => 'view_balance_scorecard',
                        'route'    => ['performance.bsc', ['param'=> 1]],
                    ],
                ],
            ],
        */
        ],
    ],
];
