<?php

namespace Pointless\Menu;

use Illuminate\Support\ServiceProvider as LaravelServiceProvider;

class ServiceProvider extends LaravelServiceProvider
{
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/config/menu.php', 'pointless.menu');
    }

    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/views', 'menu');

        $this->publishes([
            __DIR__ . '/config/menu.php' => config_path('pointless/menu.php'),
            __DIR__ . '/views' => resource_path('views/vendor/menu'),
        ]);

        foreach (config('pointless.menu.layout', []) as $layoutView => $menus) {
            $this->app['view']->composer($layoutView, function ($view) use ($menus) {
                $menu = collect($menus)->flip()->map(function ($_, $menu) {
                    return $this->app->make(Menu::class)->load($menu, $this->getMenuItems($menu));
                });
                if ($activeMenu = $menu->first->hasActiveItem()) {
                    $title = $activeMenu->activeTitle();
                    $path = $activeMenu->activeItems()->pluck('url', 'title');
                }
                $view->with([
                    'menu' => $menu,
                    'defaultTitle' => empty($title) ? null : last($title),
                    'defaultPath' => $path ?? [],
                ]);
            });
        }
    }

    protected function getMenuItems($menu)
    {
        $menuItems = config("pointless.menu.menus.{$menu}", []);
        if (is_callable($menuItems) || is_string($menuItems)) {
            $menuItems = $this->app->call($menuItems);
        }
        return $menuItems;
    }
}
