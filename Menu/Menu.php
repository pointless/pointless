<?php

namespace Pointless\Menu;

use Illuminate\Contracts\Support\Htmlable;
use Illuminate\Http\Request;
use Illuminate\View\Factory;

class Menu implements Htmlable
{
    protected $view;
    protected $request;
    protected $template;

    protected $items;
    protected $active;
    protected $activeItems;

    public function __construct(Factory $view, Request $request)
    {
        $this->view = $view;
        $this->request = $request;
    }

    public function load($template, $items = [])
    {
        $this->template = $template;
        return $this->setItems($items)->setActiveItems();
    }

    public function setItems(array $items)
    {
        $this->items = Item::populateItems($this, $items);
        return $this;
    }

    public function render($customView = null, $data = [])
    {
        return $this->view->make(
            $customView ? $customView : 'menu::' . $this->template,
            array_merge(['items' => $this->items], $data)
        )->render();
    }

    public function toHtml()
    {
        return $this->render();
    }

    public function hasActiveItem()
    {
        return $this->activeItems->isNotEmpty();
    }

    public function activeTitle()
    {
        return $this->activeItems->map->title->toArray();
    }

    public function activeItems()
    {
        return $this->activeItems;
    }

    protected function setActiveItems()
    {
        $this->activeItems = $this->activeMenuItems($this->items);
        return $this;
    }

    protected function activeMenuItems($items)
    {
        $path = collect();
        if ($item = $items->first->isActive) {
            $path = $path->push($item);
            if ($item->withItems()) {
                $path = $path->merge($this->activeMenuItems($item->items));
            }
        }
        return $path;
    }

    public function request()
    {
        return $this->request;
    }
}
