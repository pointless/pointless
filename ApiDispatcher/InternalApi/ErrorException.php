<?php

namespace Pointless\ApiDispatcher\InternalApi;

use Pointless\ApiDispatcher\ApiErrorException;

class ErrorException extends ApiErrorException
{
    public $response;

    public function __construct($response)
    {
        $this->response = $response;

        parent::__construct(implode(' ', [
            "Internal request failed with status code {$response->status()}.",
            $response->exception ? '[' . get_class($response->exception) . ']' : null,
            $response->exception ? $response->exception->getMessage() : (string) $response,
        ]));
    }

    public function getResponse()
    {
        return $this->response;
    }

    public function getResponseData()
    {
        return $this->getResponse()->getData();
    }

    public function getResponseRaw()
    {
        return $this->getResponse()->getContent();
    }
}
