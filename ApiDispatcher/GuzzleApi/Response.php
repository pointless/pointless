<?php

namespace Pointless\ApiDispatcher\GuzzleApi;

use Pointless\ApiDispatcher\Response as ApiResponse;

class Response extends ApiResponse
{
    public function raw()
    {
        return $this->baseResponse->getBody();
    }
}