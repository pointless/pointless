<?php

namespace Pointless\ApiDispatcher\GuzzleApi;

use GuzzleHttp\Client as Guzzle;
use Pointless\ApiDispatcher\ApiClient;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use GuzzleHttp\Exception\ClientException as GuzzleClientException;

class Client implements ApiClient
{
    protected $configs;

    public function __construct($configs = [])
    {
        $this->configs = array_replace_recursive([
            'http_errors' => true,
            'timeout' => 30,
            'headers' => [
                'Accept' => 'application/json',
                'X-Requested-With' => 'XMLHttpRequest',
            ],
        ], $configs);
    }

    public function config($key, $value = null)
    {
        if (is_array($key)) {
            $this->configs = array_replace_recursive($this->configs, $key);
        } else {
            array_set($this->configs, $key, $value);
        }
        return $this;
    }

    public function sendRequest($method, $uri, array $data = [], array $headers = [])
    {
        $configs = array_replace_recursive($this->configs, array_merge([
            'headers' => $headers,
        ], (strtoupper($method) === 'GET') ? [] : ['json' => $data]));
        if (strtoupper($method) === 'GET' && $data) {
            $uri = url_params($uri, $data);
        }
        try {
            $response = (new Guzzle($configs))->request($method, $uri);
            return Response::fromBaseResponse($response);
        } catch(GuzzleClientException $e) {
            if ($e->getResponse()->getStatusCode() === 404) {
                abort(404, $e->getResponse()->getReasonPhrase());
            } else {
                throw new ErrorException($e);
            }
        } catch (GuzzleException $e) {
            throw new NetworkException($e);
        }
    }

}