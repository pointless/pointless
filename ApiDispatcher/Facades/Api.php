<?php

namespace Pointless\ApiDispatcher\Facades;

use Illuminate\Support\Facades\Facade;
use Pointless\ApiDispatcher\Dispatcher;

class Api extends Facade
{
    protected static function getFacadeAccessor()
    {
        return Dispatcher::class;
    }
}
