<?php

namespace Pointless\Restful\Http;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Pointless\Restful\Http\Traits\ReturnsJsonResponse;

abstract class RestfulController extends Controller
{
    use ReturnsJsonResponse;
    
    protected $request;
    protected $repository;

    abstract protected function repository(Request $request);

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->request = $request;
            $this->loadRepository($request);
            return $next($request);
        });
    }

    protected function loadRepository(Request $request)
    {
        $this->repository = $this->repository($request);
        $this->applyFilters($this->extractFilters($request));
        $this->applySorts($this->extractSorts($request));
    }

    protected function extractFilters(Request $request)
    {
        return $request->query('filters', []);
    }

    protected function extractSorts(Request $request)
    {
        $sorts = $request->query('sorts', []);
        if ($sorts === array_values($sorts)) { // [ 'gender:asc', 'age:desc' ]
            $sorts = collect($sorts)->mapWithKeys(function ($sort) {
                $parts = explode(':', $sort, 2);
                return [$parts[0] => $parts[1] ?? 'asc'];
            })->toArray();
        }
        return $sorts;
    }

    protected function applyFilters(array $filters)
    {
        collect($filters)->each(function ($filter, $attribute) {
            $customFilterMethod = 'filter' . studly_case(str_replace('.', '_', $attribute));
            if (method_exists($this, $customFilterMethod)) {
                $this->$customFilterMethod($filter);
            } else {
                $this->repository->applyFilter($attribute, $filter);
            }
        });
    }

    protected function applySorts(array $sorts)
    {
        collect($sorts)->each(function ($direction, $attribute) {
            $customSortMethod = 'sort' . studly_case(str_replace('.', '_', $attribute));
            if (method_exists($this, $customSortMethod)) {
                $this->$customSortMethod($direction);
            } else {
                $this->repository->applySort($attribute, $direction);
            }
        });
    }

}
