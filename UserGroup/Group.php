<?php

namespace Pointless\UserGroup;

use Cache;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Support\Arrayable;
use Pointless\Author\Traits\CaptureAuthors;
use Pointless\UserGroup\Traits\HasPermissions;

class Group extends Model
{
    use CaptureAuthors, HasPermissions {
        getPoliciesAttribute as getDirectPoliciesAttribute;
    }

    protected $fillable = [
        'code',
        'name',
        'type',
        'parent_id',
        'inherit_permissions',
    ];

    protected static function boot()
    {
        parent::boot();

        static::saving(function ($group) {
            Cache::tags('groups')->flush();
        });

        static::deleted(function ($group) {
            Cache::tags('groups')->flush();
        });
    }

    public static function findByCode($code)
    {
        if (is_array($code) || $code instanceof Arrayable) {
            return static::whereIn('code', $code)->get();
        }
        return static::where('code', $code)->firstOrFail();
    }

    public function users()
    {
        return $this->belongsToMany(config('auth.providers.users.model'));
    }

    public function members()
    {
        return $this->users()->orderBy('rank')->orderBy('id');
    }

    public function ancestorGroups()
    {
        return $this->parentGroup()->with('ancestorGroups');
    }

    public function parentGroup()
    {
        return $this->belongsTo(static::class, 'parent_id');
    }

    public function includeAncestors()
    {
        return Cache::tags(['groups'])->rememberForever("group:ancestors:{$this->code}", function () {
            $groups = $this->newCollection([($group = $this)]);
            while ($parentGroup = $group->parentGroup) {
                $groups->push($parentGroup);
                $group = $parentGroup;
            }
            return $groups;
        });
    }

    public function includeDescendants()
    {
        return Cache::tags(['groups'])->rememberForever("group:descendants:{$this->code}", function () {
            return $this->descendantGroups->reduce(function ($groups, $group) {
                return $groups->merge($group->includeDescendants());
            }, $this->newCollection([$this]));
        });
    }

    public function subGroups()
    {
        return $this->hasMany(static::class, 'parent_id');
    }

    public function descendantGroups()
    {
        return $this->subGroups()->with('descendantGroups');
    }

    public function getLeaderAttribute()
    {
        return $this->members()->first();
    }

    public function getPoliciesAttribute()
    {
        return Cache::tags(['groups'])->rememberForever("group:policies:{$this->code}", function () {
            if ($this->inherit_permissions && $this->parentGroup) {
                $policies = $this->parentGroup->policies;
            }
            return collect($policies ?? [])->merge($this->direct_policies)->unique()->toArray();
        });
    }

}
