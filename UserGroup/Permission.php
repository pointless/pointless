<?php

namespace Pointless\UserGroup;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
	public $timestamps = false;

	protected $touches = ['authorizable'];

    protected $fillable = [
        'policy',
    ];

    public function authorizable()
    {
    	return $this->morphTo();
    }

}
