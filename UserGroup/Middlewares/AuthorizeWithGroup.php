<?php

namespace Pointless\UserGroup\Middlewares;

use Closure;
use Illuminate\Contracts\Auth\Factory as Auth;
use Illuminate\Auth\Access\AuthorizationException;

class AuthorizeWithGroup
{
    protected $auth;

    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next, $group)
    {
        $this->auth->authenticate();

        if (! $this->auth->user()->inGroup($group)) {
            throw new AuthorizationException(trans('auth.access_denied'));
        }

        return $next($request);
    }

}
