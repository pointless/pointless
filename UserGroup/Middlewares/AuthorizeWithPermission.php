<?php

namespace Pointless\UserGroup\Middlewares;

use Closure;
use Illuminate\Contracts\Auth\Factory as Auth;
use Illuminate\Auth\Access\AuthorizationException;

class AuthorizeWithPermission
{
    protected $auth;

    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next, ...$permissions)
    {
        $this->auth->authenticate();

        if (! $this->auth->user()->hasEitherPermission(...$permissions)) {
            throw new AuthorizationException(trans('auth.access_denied'));
        }

        return $next($request);
    }

}
