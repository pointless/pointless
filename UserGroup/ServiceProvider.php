<?php

namespace Pointless\UserGroup;

use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider as LaravelServiceProvider;

class ServiceProvider extends LaravelServiceProvider
{

    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/config/permissions.php', 'pointless.permissions');

    }

    public function boot()
    {
        $this->publishes([
            __DIR__ . '/config/permissions.php' => config_path('pointless/permissions.php'),
        ]);

        $this->loadMigrationsFrom(__DIR__ . '/migrations');

        $this->registerPermissions();
        $this->registerBladeDirectives();
    }

    protected function registerPermissions()
    {
        Gate::before(function ($user, $ability) {
            return $user->hasPermission($ability);
        });
    }

    protected function registerBladeDirectives()
    {

        Blade::if('group', function (...$group) {
            return auth()->user()->inGroup(...$group);
        });

        $groupTypes = $this->app['config']->get('pointless.permissions.group_types', []);

        foreach ($groupTypes as $groupType) {
            Blade::if(camel_case($groupType), function (...$group) {
                return auth()->user()->inGroup(...$group);
            });
        }
    }

}
