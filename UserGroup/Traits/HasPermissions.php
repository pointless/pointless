<?php

namespace Pointless\UserGroup\Traits;

use Pointless\UserGroup\Permission;

trait HasPermissions
{

    public function permissions()
    {
        return $this->morphMany(Permission::class, 'authorizable');
    }

    public function hasPermission(string ...$policies)
    {
        return collect($policies)->diff($this->policies)->isEmpty();
    }

    public function hasEitherPermission(string ...$policies)
    {
        return collect($policies)->intersect($this->policies)->isNotEmpty();
    }

    public function addPermissions(string ...$policies)
    {
        collect($policies)->diff($this->permissions->pluck('policy'))
            ->each(function ($policy) {
                $this->permissions()->create(compact('policy'));
            });
        return $this->load('permissions');
    }

    public function removePermissions(string ...$policies)
    {
        $this->permissions()->whereIn('policy', $policies)->delete();
        return $this->load('permissions');
    }

    public function syncPermissions(string ...$policies)
    {
        $this->permissions()->delete();
        return $this->addPermissions(...$policies);
    }

    public function getPoliciesAttribute()
    {
        return $this->permissions->pluck('policy')->toArray();
    }

}