<?php

namespace Pointless\UserGroup\Traits;

use Pointless\UserGroup\Group;

trait HasGroupPermissions
{
    use HasPermissions {
        getPoliciesAttribute as getDirectPoliciesAttribute;
    }

    protected static function getGroupModelClass()
    {
        return config('pointless.permissions.group_model', Group::class);
    }

    public function getPoliciesAttribute()
    {
        return $this->groups->map->policies->flatten()
            ->merge($this->direct_policies)->unique()->toArray();
    }

    public function groups()
    {
        return $this->belongsToMany(static::getGroupModelClass())
            ->as('appointment')
            ->withPivot('rank')
            ->withTimestamps();
    }

    public function getInheritedGroups()
    {
        return $this->groups->map->includeAncestors()->flatten();
    }

    public function inGroup(string ...$groups)
    {
        return collect($groups)->diff($this->getInheritedGroups()->pluck('code'))->isEmpty();
    }

    public function inEitherGroup(string ...$groups)
    {
        return collect($groups)->intersect($this->getInheritedGroups()->pluck('code'))->isNotEmpty();
    }

    public function inExactGroup(string ...$groups)
    {
        return collect($groups)->diff($this->groups->pluck('code'))->isEmpty();
    }

    public function inEitherExactGroup(string ...$groups)
    {
        return collect($groups)->intersect($this->groups->pluck('code'))->isNotEmpty();
    }

    public function removeFromGroup(string ...$groups)
    {
        $groups = (static::getGroupModelClass())::findByCode($groups)
            ->map->includeDescendants()->flatten();
        $this->groups()->detach($groups->pluck('id'));
        return $this->load('groups');
    }

    public function removeFromExactGroup(string ...$groups)
    {
        $this->groups()->detach((static::getGroupModelClass())::findByCode($groups));
        return $this->load('groups');
    }

    public function addToGroup(string $group, $rank = null)
    {
        $groupId = (static::getGroupModelClass())::findByCode($group)->getKey();
        $this->groups()->syncWithoutDetaching([$groupId]);
        if ($rank !== null) {
            $this->groups()->updateExistingPivot($groupId, compact('rank'));
        }
        return $this->load('groups');
    }

    public function addToGroups(string ...$groups)
    {
        $this->groups()->syncWithoutDetaching((static::getGroupModelClass())::findByCode($groups)->pluck('id'));
        return $this->load('groups');
    }

    public function scopeFromGroup($query, string ...$groups)
    {
        (static::getGroupModelClass())::findByCode($groups)->map->includeDescendants()
            ->each(function ($groups) use ($query) {
                $query->whereHas('groups', function ($query) use ($groups) {
                    $query->whereIn('code', $groups->pluck('code'));
                });
            });
    }

    public function scopeFromEitherGroup($query, string ...$groups)
    {
        $groups = (static::getGroupModelClass())::findByCode($groups)
            ->map->includeDescendants()->flatten();
        $query->whereHas('groups', function ($query) use ($groups) {
            $query->whereIn('code', $groups->pluck('code'));
        });
    }

    public function scopeFromExactGroup($query, string ...$groups)
    {
        (static::getGroupModelClass())::findByCode($groups)
            ->each(function ($group) use ($query) {
                $query->whereHas('groups', function ($query) use ($group) {
                    $query->where('code', $group->code);
                });
            });
    }

    public function scopeFromEitherExactGroup($query, string ...$groups)
    {
        $groups = (static::getGroupModelClass())::findByCode($groups);
        $query->whereHas('groups', function ($query) use ($groups) {
            $query->whereIn('code', $groups->pluck('code'));
        });
    }
}
