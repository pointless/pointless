<?php

return [

    'group_model' => \Pointless\UserGroup\Group::class,

    /*
    |--------------------------------------------------------------------------
    | Group Types
    |--------------------------------------------------------------------------
    |
    | Here you may specify what are the available group types for your app.
    | You can then use the respective blade directives in your views.
    | For example, department, role
    | @department('software') ... @enddepartment
    | @role('manager') ... @endrole
    */

    'group_types' => [
    	'role',
    	// 'branch',
    	// 'department',
    ],

    /*
    |--------------------------------------------------------------------------
    | Policies
    |--------------------------------------------------------------------------
    |
    | Here you may specify all the user/group policies in your application.
	| These policies will then be defined in the Laravel Gate Policy.
    */
    'policies' => [
    	// 'create-user' => 'Create User',
    ],


];
